# Implementation of a pointer List


class CellType:
    element = None
    next = None
    def __init__(self, element=None, next=None):
        self.element = element
        self.next = next


class PointerList:
    head = CellType()
    def __init__(self):
        self.head = CellType()

    def previous(self, p):
        it = self.head
        while it is not None:
            if it.next == p:
                return it
            it = it.next
        return None

    def locate(self, item):
        it = self.head
        while it is not None:
            if it.element == item:
                return it
            it = it.next

        return None

    def insert(self, x, p):
        if p is not None:
            if p == self.head:
                if self.head.element is not None:
                    new = CellType()
                    new.element = self.head.element
                    new.next = self.head.next
                    self.head.element = x
                    self.head.next = new

                elif self.head.element is None:
                    self.head.element = x

                else:
                    new = CellType()
                    new.element = x
                    prev = self.previous(p)
                    if prev is not None:
                        prev.next = new

                    new.next = p

            return

    def retrieve(self, p):
        if p is not None:
            return p.element
        else:
            raise Exception

    def delete(self, item):
        if item is not None:
            prev = self.previous(item)
            if prev is not None:
                prev.next = item.next
            else:
                self.head = item.next
        else:
            raise Exception

    def next(self, item):
        if item is not None:
            return item.next
        return None

    def first(self):
        return self.head

    def end(self):
        it = self.head
        while it.next is not None:
            it = it.next
        return it

    def makenull(self):
        self.head = PointerList()
        return

if __name__ == "__main__":
    pass
