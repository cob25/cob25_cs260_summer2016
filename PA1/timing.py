from timeit import timeit, Timer
from sys import stdout
from copy import deepcopy

from arraylist import *
from arraystack import *
from pointerlist import *
from pointerstack import *
import time


def printTable(x):
    """Accept a dictionary of arrays and print the data as a table"""

# Check that the data structure is
    if type(x) is not dict:
        raise 'Invalid data type received'
    for key in x:
        if type(x[key]) is not list:
            raise 'Invalid data type received'

    # Determine column widths
    widths = {}
    for key in sorted(x):
        widths[key] = len(key) + 2
        for item in x[key]:
            l = len(str(item))
            if l > widths[key]:
                widths[key] = l

    # Print headers
    for key in sorted(x):
        stdout.write(' %s ' % str(key).ljust(widths[key]))
    print

    # Print underlines
    for key in sorted(widths):
        stdout.write((widths[key] * '-') + '  ')
    print

    # Print data
    row = 0
    go = True
    while go:
        go = False
        for key in sorted(x):
            try:
                if bool(x[key][row]):
                    go = True
                    break
            except:
                pass
        if go:
            for key in sorted(x):
                try:
                    stdout.write(' ' + str(x[key][row]).ljust(widths[key] + 1))
                except:
                    stdout.write((widths[key] + 2) * ' ')
        print
        row += 1


def main():

    # ======================================================================
    # List: iterated insertion - front
    # ======================================================================

    table = {
        'Data Size': [],
        'List (Array)': [],
        'List (Pointer)': [],
        'List (Python)': []
    }

    for data_size in range(1, 101, 10):
        table['Data Size'].append(data_size)

        # Array implementation
        array = ArrayList()
        for i in range(1, 10000):
            array.append(i)

        def arrTest():
            for i in xrange(0, data_size):
                array.insert(i, 0)

        t = Timer(arrTest)
        table['List (Array)'].append('{0:.8f}'.format(t.timeit(10)))

        # Pointer implementation
        def pointTest():
            pointer = PointerList()
            for i in xrange(0, data_size):
                pointer.insert(i, pointer.head)
        t = Timer(pointTest)
        table['List (Pointer)'].append('{0:.8f}'.format(t.timeit(10)))

        # Built-in implementation
        def pyTest():
            l = []
            for i in xrange(0, data_size):
                l.insert(0, i)
        t = Timer(pyTest)
        table['List (Python)'].append('{0:.8f}'.format(t.timeit(10)))

    print '\nList: Iterated Insertion - Front\n'
    printTable(table)
    print '\n'

    # ======================================================================
    # List: iterated traversal - front
    # ======================================================================

    table = {
        'Data Size': [],
        'List (Array)': [],
        'List (Pointer)': [],
        'List (Python)': []
    }

    for data_size in range(1, 101, 10):
        table['Data Size'].append(data_size)

        # Array implementation
        def arrTest():
            array = ArrayList()
            array.append(0)

            for i in xrange(0, data_size):
                array.insert(i, array.end() + 1)
        t = Timer(arrTest)
        table['List (Array)'].append('{0:.8f}'.format(t.timeit(10)))

        # Pointer implementation
        def pointTest():
            pointer = PointerList()
            for i in xrange(0, data_size):
                pointer.insert(i, pointer.end())
        t = Timer(pointTest)
        table['List (Pointer)'].append('{0:.8f}'.format(t.timeit(10)))

        # Built-in implementation
        def pyTest():
            l = []
            for i in xrange(0, data_size):
                l.append(i)
        t = Timer(pyTest)
        table['List (Python)'].append('{0:.8f}'.format(t.timeit(10)))

    print '\nList: Iterated Insertion - Back\n'
    printTable(table)
    print '\n'

 # ======================================================================
    # List: iterated traversal - back
    # ======================================================================

    table = {
        'Data Size': [],
        'List (Array)': [],
        'List (Pointer)': [],
        'List (Python)': []
    }

    for data_size in range(1, 101, 10):
        table['Data Size'].append(data_size)

        array = ArrayList()
        for i in range(10000):
            array.append(i)

        # Array implementation
        def arrTest():
            last_next = None
            i = 0
            while i != last_next:
                try:
                    x = array.retrieve(i)
                except:
                    break
                last_next = i
                i = array.next(i)

            for i in xrange(0, data_size):
                array.insert(i, array.end() + 1)
        t = Timer(arrTest)
        table['List (Array)'].append('{0:.8f}'.format(t.timeit(10)))

        pointer = PointerList()
        for i in xrange(0, data_size):
            pointer.insert(i, pointer.end())
        # Pointer implementation

        def pointTest():
            p = pointer.head
            while p is not None:
                x = pointer.retrieve(p)
                p = pointer.next(p)

        t = Timer(pointTest)
        table['List (Pointer)'].append('{0:.8f}'.format(t.timeit(10)))

        # Built-in implementation
        l = []
        for i in xrange(0, data_size):
            l.append(i)

        def pyTest():

            for i in xrange(0, len(l)):
                x = l[i]

        t = Timer(pyTest)
        table['List (Python)'].append('{0:.8f}'.format(t.timeit(10)))

    print '\nList: Traversal\n'
    printTable(table)
    print '\n'

    #  # ======================================================================
    # # List: iterated deletion - Front
    # # ======================================================================
    #
    # table = {
    #     'Data Size': [],
    #     'List (Array)': [],
    #     'List (Pointer)': [],
    #     'List (Python)': []
    # }
    #
    # for data_size in range(1, 101, 10):
    #     table['Data Size'].append(data_size)
    #
    #     array = ArrayList()
    #     for i in range(0, data_size):
    #         array.append(i)
    #
    #     # Array implementation
    #     def arrTest():
    #         while 1:
    #             try:
    #                 array.delete(0)
    #             except:
    #                 break
    #
    #     t = Timer(arrTest)
    #     table['List (Array)'].append('{0:.8f}'.format(t.timeit(10)))
    #
    #     pointer = PointerList()
    #     for i in xrange(0, data_size):
    #         pointer.insert(i, pointer.end())
    #     # Pointer implementation
    #
    #     def pointTest():
    #         p = pointer.head
    #         while 1:
    #             try:
    #                 pointer.delete(pointer.head)
    #             except:
    #                 break
    #
    #     t = Timer(pointTest)
    #     table['List (Pointer)'].append('{0:.8f}'.format(t.timeit(10)))
    #
    #     # Built-in implementation
    #     l = []
    #     for i in xrange(0, data_size):
    #         l.append(i)
    #
    #     def pyTest():
    #
    #         while len(l) > 0:
    #             del l[0]
    #
    #     t = Timer(pyTest)
    #     table['List (Python)'].append('{0:.8f}'.format(t.timeit(10)))
    #
    # print '\nList: Iterated Deletion - Front\n'
    # printTable(table)
    # print '\n'

    # ======================================================================
    # List: iterated deletion - Back
    #  ======================================================================

    # table = {
    #     'Data Size': [],
    #     'List (Array)': [],
    #     'List (Pointer)': [],
    #     'List (Python)': []
    # }
    #
    # for data_size in range(1, 101, 10):
    #     table['Data Size'].append(data_size)
    #
    #     array = ArrayList()
    #     for i in xrange(0, data_size):
    #         array.append(0)
    #
    #     # Array implementation
    #     def arrTest():
    #         while 1:
    #             try:
    #                 array.delete(array.end() - 1)
    #             except:
    #                 break
    #
    #     t = Timer(arrTest)
    #     table['List (Array)'].append('{0:.8f}'.format(t.timeit(10)))
    #
    #     pointer = PointerList()
    #     for i in xrange(0, data_size):
    #         pointer.insert(i, pointer.end())
    #     # Pointer implementation
    #
    #     def pointTest():
    #         p = pointer.head
    #         while 1:
    #             try:
    #                 pointer.delete(pointer.end())
    #             except:
    #                 break
    #
    #     t = Timer(pointTest)
    #     table['List (Pointer)'].append('{0:.8f}'.format(t.timeit(10)))
    #
    #     # Built-in implementation
    #     l = []
    #     for i in xrange(0, data_size):
    #         l.append(i)
    #
    #     def pyTest():
    #
    #         while len(l) > 0:
    #             del l[-1]
    #
    #     t = Timer(pyTest)
    #     table['List (Python)'].append('{0:.8f}'.format(t.timeit(10)))
    #
    # print '\nList: Iterated Deletion - Front\n'
    # printTable(table)
    # print '\n'

    # ======================================================================
    # Stack: Iterated Insertion - Push
    # ======================================================================

    table = {
        'Data Size': [],
        'Stack (Array)': [],
        'Stack (Pointer)': [],
        'Stack (Python)': []
    }

    for data_size in range(1, 101, 10):
        table['Data Size'].append(data_size)

        # Array implementation

        def arrTest():
            stack = ArrayStack()
            for i in xrange(0, data_size):
                stack.push(i)

        t = Timer(arrTest)
        table['Stack (Array)'].append('{0:.8f}'.format(t.timeit(10)))

        # Pointer implementation
        def pointTest():
            pointer = Pointerstack()
            for i in xrange(0, data_size):
                pointer.push(i)

        t = Timer(pointTest)
        table['Stack (Pointer)'].append('{0:.8f}'.format(t.timeit(10)))

        # Built-in implementation
        def pyTest():
            l = []
            for i in xrange(0, data_size):
                l.append(i)
        t = Timer(pyTest)
        table['Stack (Python)'].append('{0:.8f}'.format(t.timeit(10)))

    print '\nStack: Iterated Insertion - Push\n'
    printTable(table)
    print '\n'

  # # ======================================================================
  #   # Stack: iterated deletion - Pop
  #   # ======================================================================
  #
  #   table = {
  #       'Data Size': [],
  #       'Stack (Array)': [],
  #       'Stack (Pointer)': [],
  #       'Stack (Python)': []
  #   }
  #
  #   for data_size in range(1, 101, 10):
  #       table['Data Size'].append(data_size)
  #
  #       stack = ArrayStack()
  #       for i in xrange(0, data_size):
  #           stack.push(0)
  #
  #       # Array implementation
  #       def arrTest():
  #           while 1:
  #               try:
  #                   stack.pop(0)
  #               except:
  #                   break
  #
  #           # for i in xrange(0, data_size):
  #           #     array.insert(i, array.end() + 1)
  #       t = Timer(arrTest)
  #       table['Stack (Array)'].append('{0:.8f}'.format(t.timeit(10)))
  #
  #       pointer = Pointerstack()
  #       for i in xrange(0, data_size):
  #           pointer.push(i)
  #       # Pointer implementation
  #
  #       def pointTest():
  #           p = pointer.head
  #           while 1:
  #               try:
  #                   pointer.pop()
  #               except:
  #                   break
  #
  #       t = Timer(pointTest)
  #       table['Stack (Pointer)'].append('{0:.8f}'.format(t.timeit(10)))
  #
  #       # Built-in implementation
  #       l = []
  #       for i in xrange(0, data_size):
  #           l.append(i)
  #
  #       def pyTest():
  #
  #           while len(l) > 0:
  #               l.pop()
  #
  #       t = Timer(pyTest)
  #       table['Stack (Python)'].append('{0:.8f}'.format(t.timeit(10)))
  #
  #   print '\nStack: Iterated Deletion - Pop\n'
  #   printTable(table)
  #   print '\n'




if __name__ == "__main__":
    main()
