# Implementation of an array Stack


class ArrayStack():
    def __init__(self):
        self.items = []

    def isEmpty(self):
        if self.items == None:
            return True
        else:
            return False

    def pop(self):
        if self.isEmpty():
            raise RuntimeError("Empty stack.")

        topIdx = len(self.items) - 1
        item = self.items[topIdx]
        del self.items[topIdx]
        return item

    def push(self, item):
        self.items.append(item)

    def top(self):
        if self.isEmpty():
            raise RuntimeError("Empty stack.")

        topIdx = len(self.items) - 1
        return self.items[topIdx]

    def empty(self):
        return len(self.items) == 0

    def makenull(self):
        self.items = []
