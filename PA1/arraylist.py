from array import *
# Implementation of an Array List


class ArrayList:
    """ This is an array implementation of a list L
    """

    def __init__(self, size=10):
        """
        Initialize the list, keeping track of the number of items in it and setting the initial value to 0
        :param size: Initial number of locations for the list object
        :return: None
        """
        self.items = []
        self.numItems = 0
        self.last = -1
        self.size = size
        self.elements = array('i')
        self.MAX_LENGTH = 10000

    def __makeroom(self):
        newlen = (self.size // 4) + self.size + 1
        newlst = []
        for i in range(self.numItems):
            newlst[i] = self.items[i]

        self.items = newlst
        self.size = newlen

    def append(self, item):
        """
        Add an item to the end of the list
        :param item: The item to add to be added
        :return: None
        """
        self.items.append(item)

        if self.numItems == len(self.items):
            newlist = [None] * self.numItems * 2
            for k in range(len(self.items)):
                newlist[k] = self.items[k]

            self.items = newlist

        self.items[self.numItems] = item
        self.numItems += 1

    def getsize(self):
        """
        Finds the current size of the list
        :return: integer
        """
        return self.numItems

    def getitem(self, position):
        """
        Returns item at the given position
        :param position: index in list
        :return: item in list
        """
        return self.items[position]

    def first(self):
        """
        This function will return the item in the first position of L.
        :return: Item in items[0], or END(L)
        """
        if self.getsize != 0:
            return self.getitem(0)
        else:
            return None

    def end(self):
        """
        Finds the item at the last position in the list
        :return: Last item in the list
        """
        if self.getsize() != 0:
            return self.getitem(-1)
        else:
            return None

    def retrieve(self, position):
        """
        Finds the item at a given index
        :param position: index in list
        :return: item in list
        """
        if self.getsize() != 0:
            return self.items[position]
        else:
            return None

    def locate(self, x):
        """
        Finds first iteration of
        :param x:
        :return:
        """
        if self.getsize() != 0:
            for i in self.items:
                looking = self.items[i]
                if looking == x:
                    return self.items[i]

                if i == self.end():
                    return self.end()
        else:
            return None

    def next(self, position):
        if position < self.last and position >= 0:
            return position+1
        else:
            return self.end()

    def previous(self, position):
        position -= 1
        return self.getitem(position)

    def insert(self, x, position):
        if not(self.last < self.MAX_LENGTH -1):
            print "List-Array.insert: Array already at maximum size: ", self.MAX_LENGTH
        elif position <= self.last+1 and position >= 0:
          # Insert x into p by moving all elements at p or after up by one
          # Start from the end of the list and move towards the back
            for i in range(self.last, position-1, -1):  # downto and including position
                self.items[i+1] = self.items[i]
        # Put x in
            self.items[position] = x
            self.last += 1
        # else:
        #     print "List-Array.insert: Index out of range: ", position, ". Length: ", self.last+1
        return

    def delete(self, position):
        """
        Delete the element in position P in the list L
        :param position: The position in P. If P does not exist in L, or if P = END(L), the result is undefined.
        :return: None
        """
        if position <= self.last and position >= 0:
            for i in range(position + 1, self.last + 1):
                self.elements[i-1] = self.elements[i]

            self.last -= 1

        return

    def makenull(self):
        self.items = [None]
        return self.end()

    def printlist(self):
        for item in self.items:
            print item


if __name__ == "__main__":
    myArray = ArrayList()
    for i in range(20):
        myArray.append(i)

    print myArray.first()
    print myArray.end()
    myArray.delete(1)
    myArray.append(1)
    
    myArray.printlist()
